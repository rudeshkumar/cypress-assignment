FROM cypress/base

RUN apt-get update && apt-get install -y xvfb

WORKDIR /app

COPY package*.json ./

RUN npm install

RUN npm install cypress

COPY . .

EXPOSE 8080

CMD ["npm", "run", "cy:run"]
