# Assignment

Cypress UI and API Automation

## Description:

- This is the project to automate UI and API (GET/POST/PUT/PATCH/DELETE) functionality using Cypress Automation tool.
- For generating reports we are using mochawesome report generator.
- Also here the project vertualized on docker container to extract and preserve reports.
- Using Gitlab we have created CI-CD pipeline
- Tests are implemented using Cypress Automation tool followed by page object model design pattern, however feature file using Gherkin tag is provieded as a sample.
- This project includes both UI and API tests, both can be run over Cypress and on Docker container
- In UI tests, scenarios are included for positive and negative tests to test the application functionality
- In API tests, scenarios are created to test GET/POST/PUT/PATCH/DELETE presponse.

#### NOTE :

- Reports are saved in mochawesome-report folder

## Installation

- Clone the repository
- Install NPM in that folder repo on your local machine: $npm install
- Install Cypress: $npm install cypress --save-dev

## Commands

```bash
## Running Cypress tests via runner locally
  $ npx cypress open

## Running Docker command
  $ docker run -it --rm -v $(pwd)/cypress/reports:/app/cypress/reports cypress-tests

```

## Features

## Sample Feature file

Features : Find Bugs application

Background : Given User pass the URL "https://academybugs.com/find-bugs/#"

    Scenario: Open URL and validate homepage loaded
        When User opens the URL
        Then Page should be loaded successfully
        And Page should contain the heading "Find Bugs"

    Scenario: Validate Bug pop up on Item view option
        When User on find Bugs Page
        And Item view options are visible and enable
        When User select item view option "10"
        Then Bug alert pop up should be visible

    Scenario: Select item from list and place order
        When User select item from the list
        And User add the item to cart
        Then Validate cart is updated
        When User checkout the order
        And Fill the user information
        When User user cpontinue to payment
        Then Verify billing and shipping information
        When User submit the order
        Then Verify "Not a real order" pop up

    Scenario : Select item and add to cart
        When User click on "ADD TO CART" buttom from selected item
        Then Verify the success message "Product successfully added to your cart."
        And User opens the cart
        Then Verify item got added to the cart

    Scenario : Add comment below the selected item
        When User select item from the list
        And Add comment below the item by filling the user information
        When User click on Post Comment buttom
        Then Verfify comment got added below the item with user information

    Scenario : Search for valid item  
        When User select item from the list 
        And Search for the valid item
        Then Verify search result 
        Then Verify search item name in the search result 

    Scenario : Add comment below the selected item without filling mandatory fields
        When User select item from the list
        And Add comment below the item without filling mandatory fields
        When User click on Post Comment buttom
        Then Verfify Error popup message

    Scenario : Verify invalid/blank coupon, gift card
        When User select item from the list
        And User add the item to cart
        When User Redeem Card without entering Reedem card Code
        Then Verify error message "Not a valid gift card number"
        When  User Apply a blank coupon
        Then Verify error message "Not a valid coupon code"

    Scenario : Verify error message for invalid email and phone number
        When User select item from the list
        And User add the item to cart
        Then Validate cart is updated
        When User checkout the order
        And Fill the user invalid email and phone number
        When User continue to payment
        Then Verify error message "Please enter a valid phone number"
        Then Verify error message "Please enter a valid Email"

    Scenario : Search for invalid item  
        When User select item from the list 
        And Search for the valid item
        Then Verify search result     
