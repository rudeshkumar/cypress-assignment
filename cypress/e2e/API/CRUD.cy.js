const data = require("../../support/constants");
import { authToken } from "../../utility/auth";

let Token, id;

describe("CRUD Tests", () => {

    before(() => {
        authToken().then((token) => {
        Token = token;
        });
    });

    it('GET request' , () => {
        cy.request({
            method : 'GET',
            url : data.Constant.API_BASE_URL,
            headers : {
                Authorization: `Bearer ${Token}`,
            }
        }).then((resp) => {
            expect(resp.status).to.equal(200)
        })
    })

    it("POST request", () => {
        cy.request({
            method: "POST",
            url: data.Constant.API_BASE_URL,
            headers: {
                Authorization: `Bearer ${Token}`,
            },
            body: {
                firstname: "Jim",
                lastname: "Brown",
                totalprice: 111,
                depositpaid: true,
                bookingdates: {
                    checkin: "2018-01-01",
                    checkout: "2019-01-01",
                },
                additionalneeds: "Breakfast",
            },
        }).then((response) => {
           expect(response.status).to.equal(200);
           id  = response.body.bookingid
           expect(response.body.booking).has.property('firstname', 'Jim')
           expect(response.body.booking).has.property('lastname', 'Brown')
        }).then(() => {
            cy.request({
                method : 'GET',
                url : data.Constant.API_BASE_URL+id,
                headers : {
                    Authorization: `Bearer ${Token}`,
                }
            }).then((res) => {
                expect(res.status).to.equal(200)
                expect(res.body).has.property('firstname', 'Jim')
                expect(res.body).has.property('lastname', 'Brown')    
            })
        })
    })

    it('PUT request' , () => {
        cy.request({
            method : 'PUT',
            url : data.Constant.API_BASE_URL+id,
            headers : {
                Authorization: `Bearer ${Token}`,
                Cookie : `token=${Token}`,
            },
            body: {
                firstname: "Linus",
                lastname: "Safron",
                totalprice: 111,
                depositpaid: true,
                bookingdates: {
                    checkin: "2018-01-01",
                    checkout: "2019-01-01",
                },
              additionalneeds: "Breakfast",
            },
        }).then((resp) => {
            expect(resp.status).to.equal(200)
            expect(resp.body).has.property('firstname', 'Linus')
            expect(resp.body).has.property('lastname', 'Safron') 
        })
    })

    it('PATCH request' , () => {
        cy.request({
            method : 'PATCH',
            url : data.Constant.API_BASE_URL+id,
            headers : {
                Authorization: `Bearer ${Token}`,
                Cookie : `token=${Token}`,
            },
            body: {
                firstname: "Tom",
                lastname: "White",
            },
        }).then((resp) => {
            expect(resp.status).to.equal(200)
            expect(resp.body).has.property('firstname', 'Tom')
            expect(resp.body).has.property('lastname', 'White') 
        })
    })

    it('DELETE request' , () => {
        cy.request({
            method : 'DELETE',
            url : data.Constant.API_BASE_URL+id,
            headers : {
                Authorization: `Bearer ${Token}`,
                Cookie : `token=${Token}`,
            }
        }).then((resp) => {
            expect(resp.status).to.equal(201)
        })
    })
})


