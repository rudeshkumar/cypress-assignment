/// <reference types = "Cypress" />
import { homePage } from "../../Pages/homePage_PO";
import { randomString } from "../../utility/random";
const data = require("../../support/constants");

const random = randomString(30);
const Homepage = homePage;
const viewOption = 10;
const item = "Blue Hoodie";

beforeEach(() => {
  Homepage.visitUrl(data.Constant.BASE_URL);
});

describe("Find Bugs application - Positive Tests", () => {
  it("Open URL and validate homepage loaded", () => {
    Homepage.validateHomePage();
  });

  it("Validate Bug pop up on Item view option", () => {
    Homepage.selectViewOption(viewOption);
    Homepage.validateBugPopup();
  });

  it("Select item and add to cart", () => {
    Homepage.addItemToCart(item);
    Homepage.verifyItemAddedToCart(data.Constant.cartsuccess);
    Homepage.clickViewCart();
    Homepage.verifyItemInCart(item);
  });

  it("Select item from list and place order", () => {
    Homepage.selectItemFromList(item);
    Homepage.addToCart(item);
    Homepage.checkoutOrder();
    Homepage.fillUserInformation(data.Constant);
    Homepage.continuePayment();
    Homepage.reviewYourCart(item);
    Homepage.verifyBillingInfo();
    Homepage.verifyShippingInfo();
    Homepage.submitOrder();
  });

  it("Add comment below the selected item", () => {
    Homepage.selectItemFromList(item);
    Homepage.addComment(random);
    Homepage.enterName(data.Constant.firstname);
    Homepage.enterEmail(data.Constant.email);
    Homepage.enterWebsite(data.Constant.BASE_URL);
    Homepage.clickPostComment();
    Homepage.verifyCommentAdded(random, data.Constant.firstname);
  });

  it("Search for valid item", () => {
    Homepage.selectItemFromList(item);
    Homepage.searchItem(item)
    Homepage.verifySearchItem(item)
    Homepage.verifyValidSearchResult(data.Constant.validsearch)
  })
});

describe("Find Bugs application - Negative Tests", () => {
  it("Add comment below the selected item without filling mandatory fields", () => {
    Homepage.selectItemFromList(item);
    Homepage.addComment(random);
    Homepage.enterWebsite(data.Constant.BASE_URL);
    Homepage.clickPostComment();
    Homepage.verifyCommentErrorPopUp();
  });

  it("Verify invalid/blank coupon, gift card", () => {
    Homepage.selectItemFromList(item);
    Homepage.addToCart(item);
    Homepage.clickRedeemCard();
    Homepage.clickApplyCoupon();
    Homepage.VerifyErrorOnRedeemCard(data.Constant.redeemerror);
    Homepage.verifyErrorOnCouponCode(data.Constant.couponerror);
  });

  it("Verify error message for invalid email and phone number", () => {
    Homepage.selectItemFromList(item);
    Homepage.addToCart(item);
    Homepage.checkoutOrder();
    Homepage.fillUserInformation(data.Constant);
    Homepage.fillInvalidContact(data.Constant);
    Homepage.continuePayment();
    Homepage.verifyInvalidEmail(data.Constant.invalidemailerror);
    Homepage.verifyInvalidPhone(data.Constant.invalidphoneerror);
  });

  it("Search for invalid item", () => {
    Homepage.selectItemFromList(item);
    Homepage.searchItem(random)
    Homepage.verifyInvalidSearchResult(data.Constant.invalidsearch)
  })

});
