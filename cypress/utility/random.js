export function randomString(length) {
  let random = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    random += characters.charAt(randomIndex);
  }
  return random;
}

export function auth() {

}