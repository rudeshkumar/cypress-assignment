export class Constant {
  static API_BASE_URL = "https://restful-booker.herokuapp.com/booking/";
  static BASE_URL = "https://academybugs.com/find-bugs/#";
  static url = 'find-bugs';
  static title = 'Find Bugs – AcademyBugs.com';
  static heading = 'Find Bugs';
  static view_option_Bug = "#1 Awesome! You found a bug. Pretty easy right?";
  static firstname = "Tamas";
  static lastname = "Turk";
  static country = "India";
  static address = "Dehli";
  static companyname = "tft";
  static city = "Delhi";
  static state = "Delhi";
  static zip = "123456";
  static phone = "9999900000";
  static email = "tamasturk@blondmail.com";
  static state_error = "Please enter your State";
  static checkout_error = "Please correct the errors in your checkout details";
  static invalidphone = "0000000000000000";
  static invalidemailid = "abcdefgh";
  static cartsuccess = "Product successfully added to your cart.";
  static redeemerror = "Not a valid gift card number";
  static couponerror = "Not a valid coupon code";
  static invalidemailerror = "Please enter a valid Email";
  static invalidphoneerror = "Please enter a valid phone number";
  static validsearch = "Showing 1 result";
  static invalidsearch = "No Results Found";


}
