const data = require("../support/constants");
export const homePage = {
  heading: "main#main>article>div>h3",
  viewOption: (view) => `section#ec_product_page>div>span>a:contains(${view})`,
  loaderText: ".academy-bug-overlay > h5",
  bugPopup: "html>body>div:nth-of-type(6)>div div h5",
  selectItem: (item) =>
    `ul li div.academy-product-description-wrapper h3:contains(${item})`,
  itemName: "h1.ec_details_title",
  addToCartButton: 'input[value="ADD TO CART"]',
  itemInCart: ".ec_cart_left.ec_cart_holder",
  checkoutButton: "a.ec_cart_button.academy-checkout-bug",
  selectCountry: "#ec_cart_billing_country",
  firstName: "input#ec_cart_billing_first_name",
  lastName: "input#ec_cart_billing_last_name",
  companyName: "input#ec_cart_billing_company_name",
  address: "input#ec_cart_billing_address",
  city: "input#ec_cart_billing_city",
  zip: "input#ec_cart_billing_zip",
  state: "#ec_cart_billing_state_IN",
  phone: "input#ec_cart_billing_phone",
  email: "input#ec_contact_email",
  confirmEmail: "input#ec_contact_email_retype",
  continuePaymentbutton: ".ec_cart_button_row.ec_show_two_column_only",
  stateError: "div#ec_cart_billing_state_error",
  checkoutError: "div#ec_checkout2_error",
  cartReview: ".ec_cart_price_row_cartitem_0 > .ec_cart_price_row_label",
  billingFullName: "div#ec_cart_payment_hide_column>div:nth-of-type(2)",
  billingEmail: "div#ec_cart_payment_hide_column>div:nth-of-type(4)",
  billingPhone: "div#ec_cart_payment_hide_column>div:nth-of-type(7)",
  billingCountry: "div#ec_cart_payment_hide_column>div:nth-of-type(6)",
  shippingFullName: "div#ec_cart_payment_hide_column>div:nth-of-type(10)",
  shippingCountry: "div#ec_cart_payment_hide_column>div:nth-of-type(14)",
  shippingPhone: "div#ec_cart_payment_hide_column>div:nth-of-type(15)",
  submitOrderButton: "input.ec_cart_button",
  notArealOrder: "html>body>div:nth-of-type(10)>div",
  loader: "main#main>article>div>script",
  addToCartButtton: (item) => `.ec_product_li:contains(${item})`,
  itemAddedToCart: ".ec_product_added_to_cart",
  viewCartButton: "div.ec_product_added_to_cart a ",
  itemNameInCart: ".ec_cartitem_details",
  commentInput: 'textarea[name="comment"]',
  nameInput: 'input[name="author"]',
  emailInput: 'input[name="email"]',
  websiteInput: 'input[name="url"]',
  postCommentButton: "#submit",
  commentText: ".comment-content",
  nameOnComment: "a.url",
  errorPopup: "body#error-page",
  redeemButton: "#ec_apply_gift_card",
  couponCardbutton: "#ec_apply_coupon",
  redeemError: "#ec_gift_card_error",
  couponCardError: "#ec_coupon_error",
  emailError: "#ec_contact_email_error",
  phoneError: "#ec_cart_billing_phone_error",
  searchInput: "input.ec_search_input",
  searchButton: "input[value='Search']",
  searchItemName: "h3.test_title.ec_product_title_type1 a",
  searchResult: "span.ec_product_page_showing",
  invalidsearchResult: ".ec_products_no_results",


  visitUrl(url) {
    cy.visit(url);
    return this;
  },

  validateHomePage() {
    cy.url().should("include", data.Constant.url);
    cy.title().should("eq", data.Constant.title);
    cy.get(this.heading).should("be.visible").contains(data.Constant.heading);
    return this;
  },

  selectViewOption(view) {
    cy.get(this.viewOption(view)).click();
    return this;
  },

  validateBugPopup() {
    cy.get(this.loaderText, { timeout: 5000 }).should("not.exist");
    cy.get(this.bugPopup, { timeout: 10000 })
      .should("be.visible")
      .contains(data.Constant.view_option_Bug);
    return this;
  },

  selectItemFromList(item) {
    cy.get(this.selectItem(item)).click();
    cy.get(this.itemName, {timeout : 5000}).last().should("be.visible").contains(item);
    return this;
  },

  addToCart(item) {
    cy.get(this.addToCartButton).click();
    cy.get(this.loader).should("not.be.visible");
    cy.get(this.itemInCart, { timeout: 5000 })
      .should("be.visible")
      .contains(item);
    return this;
  },

  addItemToCart(item) {
    cy.get(this.addToCartButtton(item))
      .find(".ec_product_addtocart a")
      .first()
      .click({ force: true });
    return this;
  },

  verifyItemAddedToCart(text) {
    cy.get(this.itemAddedToCart).should("be.visible").contains(text);
    return this;
  },

  clickViewCart() {
    cy.get(this.viewCartButton).click();
    return this;
  },

  verifyItemInCart(item) {
    cy.get(this.itemNameInCart, {timeout : 5000}).should("be.visible").contains(item);
    return this;
  },

  checkoutOrder() {
    cy.get(this.checkoutButton).click();
    return this;
  },

  fillUserInformation(data) {
    cy.get(this.loader).should("not.be.visible");
    cy.get(this.selectCountry, { timeout: 10000 }).select(data.country);
    cy.get(this.firstName).type(data.firstname);
    cy.get(this.lastName).type(data.lastname);
    cy.get(this.companyName).type(data.companyname);
    cy.get(this.address).type(data.address);
    cy.get(this.city).type(data.city);
    cy.get(this.zip).type(data.zip);
    cy.get(this.state).select(data.state);
    cy.get(this.phone).type(data.phone);
    cy.get(this.email).type(data.email);
    cy.get(this.confirmEmail).type(data.email);
    return this;
  },

  searchItem(item) {
    cy.get(this.searchInput).click().type(item);
    cy.get(this.searchButton).click()
    return this;
  },

  verifySearchItem(item) {
    cy.get(this.searchItemName, {timeout : 5000}).should("be.visible").should("have.text", item)
    
  },

  verifyValidSearchResult(result) {
    cy.get(this.searchResult).should("be.visible").contains(result);
  },

  verifyInvalidSearchResult(result) {
    cy.get(this.invalidsearchResult).should("be.visible").contains(result);
    return this;
  },

  fillInvalidContact(data) {
    cy.get(this.phone).clear().type(data.invalidphone);
    cy.get(this.email).clear().type(data.invalidemailid);
    cy.get(this.confirmEmail).clear().type(data.invalidemailid);
    return this;
  },

  continuePayment() {
    cy.get(this.continuePaymentbutton).click();
    return this;
  },

  reviewYourCart(item) {
    cy.get(this.cartReview, {timeout : 5000}).should("be.visible").contains(item);
    return this;
  },

  verifyBillingInfo() {
    cy.get(this.billingFullName, { timeout : 10000})
      .should("be.visible")
      .contains(`${data.Constant.firstname} ${data.Constant.lastname}`);
    cy.get(this.billingPhone)
      .should("be.visible")
      .contains(data.Constant.phone);
    cy.get(this.billingCountry)
      .should("be.visible")
      .contains(data.Constant.country);
    return this;
  },

  verifyShippingInfo() {
    cy.get(this.shippingFullName)
      .should("be.visible")
      .contains(`${data.Constant.firstname} ${data.Constant.lastname}`);
    cy.get(this.shippingPhone)
      .should("be.visible")
      .contains(data.Constant.phone);
    cy.get(this.shippingCountry)
      .should("be.visible")
      .contains(data.Constant.country);
    return this;
  },

  submitOrder() {
    cy.get(this.submitOrderButton).click();
    return this;
  },

  verifyOrderSubmit() {
    cy.get(this.notArealOrder, {timeout : 5000}).should("be.visible");
    return this;
  },

  addComment(comment) {
    cy.get(this.commentInput).type(comment);
  },

  enterName(name) {
    cy.get(this.nameInput).type(name);
    return this;
  },

  enterEmail(email) {
    cy.get(this.emailInput).type(email);
    return this;
  },

  enterWebsite(url) {
    cy.get(this.websiteInput).type(url);
    return this;
  },

  clickPostComment() {
    cy.get(this.postCommentButton).click();
    return this;
  },

  verifyCommentAdded(comment, firstname) {
    cy.get(this.commentText).first().should("be.visible").contains(comment);
    cy.get(this.nameOnComment).should("be.visible").contains(firstname);
  },

  validateFormErrorMessage(stateError, cheeckoutError) {
    cy.get(this.stateError)
      .invoke("text")
      .then((text) => {
        const trimmedText = text.trim();
        cy.wrap(trimmedText).should("eq", stateError);
      });
    cy.get(this.checkoutError).should("be.visible").contains(cheeckoutError);
    return this;
  },

  verifyCommentErrorPopUp() {
    cy.get(this.errorPopup).should("be.visible");
    return this;
  },

  clickRedeemCard() {
    cy.get(this.redeemButton).click();
    return this;
  },

  clickApplyCoupon() {
    cy.get(this.couponCardbutton).click();
    return this;
  },

  VerifyErrorOnRedeemCard(error) {
    cy.get(this.redeemError, {timeout : 5000}).should("be.visible").should("have.text", error);
    return this;
  },

  verifyErrorOnCouponCode(error) {
    cy.get(this.couponCardError)
      .should("be.visible")
      .should("have.text", error);
    return this;
  },

  verifyInvalidEmail(error) {
    cy.get(this.emailError, {timeout : 5000}).should("be.visible").contains(error);
    return this;
  },

  verifyInvalidPhone(error) {
    cy.get(this.phoneError).should("be.visible").contains(error);
    return this;
  },
};

